<?php
return[
    'limitPagination' => 15,
    'system' => [
        'name'=> 'Gerenciamento BackEnd do LeBrechó',
        'shortName' => 'LeBrechó',
        'version' => '1.0',
        'fullVersion' => '1.0.0',
        'yearRelease' => '2017'
    ],
    'author' => [
        'name' => 'Renzo Grosso',
        'company' => 'Gros',
        'local' => '',
        'site' => '',
        'email' => '',
        'developer' => [
            'name' => 'Fábio Valentim',
            'site' => 'www.baudovalentim.net',
            'email' => 'valentim@baudovalentim.net'
        ]
    ],
    'host' => [
        'developer' => 'localhost',
        'homolog' => 'homolog.valentim.xyz',
        'release' => ''
    ]
];