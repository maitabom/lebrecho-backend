<?php
return [
    /**
     * Debug Level:
     *
     * Production Mode:
     * false: No error messages, errors, or warnings shown.
     *
     * Development Mode:
     * true: Errors and warnings shown.
     */
    'debug' => filter_var(env('DEBUG', ($_SERVER[HTTP_HOST] == \Cake\Core\Configure::read('host.developer'))), FILTER_VALIDATE_BOOLEAN)
];