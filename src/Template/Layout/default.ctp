<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta content="width=device-width" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>
        <?php
        if(isset($title))
        {
            echo $title . " | " . \Cake\Core\Configure::read('system.name');
        }
        else
        {
            echo \Cake\Core\Configure::read('system.name');
        }
        ?>
    </title>

    <?= $this->Html->meta('icon') ?>

    <!-- JavaScript -->
    <?= $this->Html->script('jquery-1.10.2.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('chartist.min.js') ?>
    <?= $this->Html->script('bootstrap-notify.js') ?>
    <?= $this->Html->script('paper-dashboard.js') ?>
    <?= $this->Html->script('demo.js') ?>
    <?= $this->Html->script('vanilla-masker.js') ?>
    <?= $this->Html->script('sweetalert2.js') ?>
    <?= $this->Html->script('bootstrap-datepicker.js') ?>
    <?= $this->Html->script('md5.js') ?>
    <?= $this->Html->script('locales/bootstrap-datepicker.pt-BR.js') ?>

    <!-- CSS -->
    <?= $this->Html->css('bootstrap.min.css'); ?>
    <?= $this->Html->css('animate.min.css'); ?>
    <?= $this->Html->css('themify-icons.css'); ?>
    <?= $this->Html->css('paper-dashboard.css'); ?>
    <?= $this->Html->css('demo.css'); ?>
    <?= $this->Html->css('sweetalert2.css'); ?>
    <?= $this->Html->css('datepicker3.css'); ?>

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
</head>
<body>
<?= $this->element('menu') ?>
<div class="main-panel">
    <?= $this->element('navbar') ?>
    <?= $this->fetch('content') ?>
    <?= $this->element('footer') ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        demo.initChartist();
    });
</script>
</body>
</html>
