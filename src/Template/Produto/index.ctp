<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Produtos</h4>
                    </div>
                    <div class="content">
                        <?php
                        echo $this->Form->create("Usuario", array(
                            "url" => array(
                                "controller" => "usuario",
                                "action" => "index"
                            ),
                            "role" => "form"));
                        ?>
                        <div class="row">
                            <div class="col-xs-6">
                                <?= $this->Form->label("codebar", "Código de Barras") ?><br/>
                                <?= $this->Form->text("codebar", array("class" => "form-control", "style" => "width: 100%")) ?>
                            </div>
                            <div class=" col-xs-6">
                                <?= $this->Form->label("nome-produto", "Nome do Produto") ?><br/>
                                <?= $this->Form->text("cliente", array("class" => "form-control", "style" => "width: 100%")) ?>
                            </div>
                            <div class=" col-xs-3">
                                <?= $this->Form->label("data-inicial", "Categoria") ?><br/>
                                <?= $this->Form->select("mostra", $categorias, ["class" => "form-control", "style" => "width: 100%", "empty" => false]) ?>
                            </div>
                            <div class=" col-xs-3">
                                <?= $this->Form->label("data-final", "Condição") ?><br/>
                                <?= $this->Form->select("mostra", $condicoes, ["class" => "form-control", "style" => "width: 100%", "empty" => false]) ?>
                            </div>
                            <div class=" col-xs-3">
                                <?= $this->Form->label("nome-produto", "Mostrar") ?><br/>
                                <?= $this->Form->select("mostra", $combo_status, ["class" => "form-control", "style" => "width: 100%", "empty" => false]) ?>
                            </div>
                            <div class=" col-xs-3">
                                <?= $this->Form->label("nome-produto", "Status") ?><br/>
                                <?= $this->Form->select("mostra", $status, ["class" => "form-control", "style" => "width: 100%", "empty" => false]) ?>
                            </div>
                        </div>
                        <div style="min-height: 15px">

                        </div>
                        <div style="text-align: right;">
                            <button type="button" id="btnNovo" class="btn btn-success"
                                    onclick="<?= 'window.location = \'' . $this->Url->build(['controller' => 'produto', 'action' => 'add']) . '\'' ?>">
                                Novo
                            </button>
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <table id="bootstrap-table" class="table table-hover" style="margin-top: 0px;">
                            <thead style="display: table-header-group;">
                            <tr>
                                <th class="" style="" data-field="id">
                                    <div class="th-inner ">Nome</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th style="" data-field="name">
                                    <div class="th-inner sortable both">Fornecedor</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th style="" data-field="salary">
                                    <div class="th-inner sortable both">Fabricante</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th style="" data-field="country">
                                    <div class="th-inner sortable both">Categoria</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th style="" data-field="country">
                                    <div class="th-inner sortable both">Quantidade</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th style="" data-field="country">
                                    <div class="th-inner sortable both">Preço</div>
                                    <div class="fht-cell"></div>
                                </th>
                                <th class="td-actions text-right" style="" data-field="actions">
                                    <div class="th-inner"></div>
                                    <div class="fht-cell"></div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="">Tablet Positivo</td>
                                <td style="">Marcos Informática</td>
                                <td style="">Positivo</td>
                                <td style="">Informática</td>
                                <td style="">10</td>
                                <td style="">R$ 250,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Óculos Rainban</td>
                                <td style="">Chilli Beans</td>
                                <td style="">Rainban</td>
                                <td style="">Acessórios</td>
                                <td style="">4</td>
                                <td style="">R$ 100,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Relógio Orient</td>
                                <td style="">Oriental Internacional</td>
                                <td style="">Orient</td>
                                <td style="">Acessórios</td>
                                <td style="">10</td>
                                <td style="">R$ 50,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Boneca Barbie</td>
                                <td style="">Venetillo</td>
                                <td style="">Mattel</td>
                                <td style="">Brinquedos</td>
                                <td style="">10</td>
                                <td style="">R$ 50,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Relógio de Parede</td>
                                <td style="">Venetillo</td>
                                <td style="">Herweg</td>
                                <td style="">Casa e Lar</td>
                                <td style="">10</td>
                                <td style="">R$ 10,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Teclado Padrão</td>
                                <td style="">Marcos Informática</td>
                                <td style="">C3 Tech</td>
                                <td style="">Informática</td>
                                <td style="">10</td>
                                <td style="">R$ 5,00</td>
                                <td class="td-actions text-right" style="">
                                    <div class="table-icons">
                                        <a rel="tooltip" title="Imagens"
                                           class="btn btn-simple btn-info btn-icon table-action view"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>">
                                            <i class="ti-camera"></i>
                                        </a>
                                        <a rel="tooltip" title="Editar"
                                           href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'edit', 1]) ?>"
                                           class="btn btn-simple btn-warning btn-icon table-action edit"><i
                                                class="ti-pencil-alt"></i></a>
                                        <a rel="tooltip" title="Remover" onclick="confirm('Deseja excluir o usuário?')"
                                           class="btn btn-simple btn-danger btn-icon table-action remove"><i
                                                class="ti-close"></i></a>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>