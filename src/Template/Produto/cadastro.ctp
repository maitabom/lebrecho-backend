<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="content">
                    <?php
                    echo $this->Form->create("Usuario", array(
                        "url" => array(
                            "controller" => "transacao",
                            "action" => "cadastro"
                        ),
                        "role" => "form"));
                    ?>
                    <div class="content">
                        <fieldset>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('cliente', 'Código de Barras') ?>
                                <?= $this->Form->text('cliente', ['class' => 'form-control']) ?>
                            </div>

                            <div class="form-group col-md-9">
                                <?= $this->Form->label('valor', 'Nome da Peça') ?>
                                <?= $this->Form->text('valor', ['class' => 'form-control']) ?>
                            </div>


                        </fieldset>
                        <fieldset>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('cliente', 'Fornecedor') ?>
                                <?= $this->Form->text('cliente', ['class' => 'form-control']) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('pedido', 'Fabricante') ?>
                                <?= $this->Form->select("mostra", $fabricantes, ["class" => "form-control", "style" => "width: 100%", "empty" => true]) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('valor', 'Categoria') ?>
                                <?= $this->Form->select("mostra", $categorias, ["class" => "form-control", "style" => "width: 100%", "empty" => true]) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('valor', 'Tags') ?>
                                <?= $this->Form->text('valor', ['class' => 'form-control']) ?>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group col-md-2">
                                <?= $this->Form->label('numero-cartao', 'Quantidade') ?>
                                <?= $this->Form->text('numero-cartao', ['class' => 'form-control']) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('codigo-seguranca', 'Preço do Fornecedor') ?>
                                <?= $this->Form->text('codigo-seguranca', ['class' => 'form-control']) ?>
                            </div>
                            <div class="form-group col-md-4">
                                <?= $this->Form->label('validade', 'Taxa a Aplicar') ?>
                                <?= $this->Form->text('validade', ['class' => 'form-control']) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('bandeira', 'Condição') ?>
                                <?= $this->Form->select("mostra", $condicoes, ["class" => "form-control", "style" => "width: 100%", "empty" => false]) ?>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-group col-md-12">
                                <?= $this->Form->label('telefone', 'Descrição') ?>
                                <?= $this->Form->textarea('telefone', ['class' => 'form-control']) ?>
                            </div>

                        </fieldset>
                        <fieldset>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('numero-cartao', 'Combinação') ?>
                                <?= $this->Form->text('numero-cartao', ['class' => 'form-control']) ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= $this->Form->label('codigo-seguranca', 'Quantidade Combinação') ?>
                                <?= $this->Form->text('codigo-seguranca', ['class' => 'form-control']) ?>
                            </div>


                        </fieldset>

                        <fieldset>
                            <div class="form-group col-xs-12">
                                <label>Outras Opções</label><br/>
                                <?= $this->Form->checkbox("ativo") ?>Ativo &nbsp;&nbsp;&nbsp;
                            </div>
                        </fieldset>

                    </div>
                    <div class="card-footer text-right">
                        <button type="reset" onclick="window.location = '<?= $this->Url->build('/produto') ?>'"
                                class="btn btn-primary">Voltar
                        </button>
                        <button type="reset" class="btn btn-primary">Limpar</button>
                        <button type="submit" class="btn btn-success">Salvar</button>
                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>