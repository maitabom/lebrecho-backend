<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="header">
                    <h4 class="title">123456789012 - Tablet Positivo</h4>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->label("imagefile", "Selecione uma imagem") ?><br/>
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 15px">

                    </div>
                    <div style="text-align: right;">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                    <hr/>
                    <h5>Imagens do Produto</h5>
                    <div class="row">
                        <div class="col-md-4">
                            <?=$this->Html->image('demo/tablet.jpg'); ?>
                        </div>
                        <div class="col-md-4">
                            <?=$this->Html->image('demo/tablet.jpg'); ?>
                        </div>
                        <div class="col-md-4">
                            <?=$this->Html->image('demo/tablet.jpg'); ?>
                        </div>
                        <div class="col-md-4">
                            <?=$this->Html->image('demo/tablet.jpg'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>