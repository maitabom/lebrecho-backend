<script type="text/javascript">
    function salvarImagens(){
        swal({
            title: "Imagens salvas com sucesso",
            text: "Imagens salvas para o produto <b>Tablet Positivo</b>",
            type: 'success',
            showCancelButton: false,
            confirmButtonClass: 'btn btn-success',
            confirmButtonText: 'OK',
        }).then(function () {
            window.location = '<?= $this->Url->build(['controller' => 'produto', 'action' => 'imagens', 1]) ?>';
        });
    }
</script>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-4">
                            <?= $this->Form->label("codigo", "Código do Produto") ?><br/>
                            <?= $this->Form->text("codigo", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                        <div class="col-xs-8">
                            <?= $this->Form->label("nome", "Nome do Produto") ?><br/>
                            <?= $this->Form->text("nome", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->label("imagefile", "Selecione uma ou mais imagens") ?><br/>
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 5px">

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 5px">

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 5px">

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 5px">

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $this->Form->file("imagefile", array("class" => "form-control", "style" => "width: 100%")) ?>
                        </div>
                    </div>
                    <div style="min-height: 15px">

                    </div>
                    <div style="text-align: right;">
                        <button type="submit" onclick="salvarImagens()" class="btn btn-primary">Enviar</button>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>