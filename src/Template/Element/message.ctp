<?php

$class = '';
$icon = '';

switch($type)
{
    case 'info':
        $class = 'alert alert-info alert-with-icon';
        $icon = 'ti-info-alt';
        break;

    case 'error':
        $class = 'alert alert-danger alert-with-icon';
        $icon = 'ti-thumb-down';
        break;

    case 'success':
        $class = 'alert alert-success alert-with-icon';
        $icon = 'ti-thumb-up';
        break;

    case 'warning':
        $class = 'alert alert-warning alert-with-icon';
        $icon = 'ti-alert';
        break;
}

?>

<div id="<?=$name?>" class="<?=$class?>" data-notify="container" style="display: none">
    <button type="button" aria-hidden="true" class="close" onclick="$(this).parent().hide('blind')">×</button>
    <span data-notify="icon" class="<?=$icon?>"></span>
    <?php if(isset($details)): ?>
        <span data-notify="message">
            <?= h($message) ?>
            &nbsp;&nbsp;<a href="#" onclick="$('#details').toggle('fade');">mais detalhes</a>
        </span>
        <div id="details" class="detalhes">
            <?= h($details) ?>
        </div>
    <?php else: ?>
        <span data-notify="message">
            <?= h($message) ?>
        </span>

    <?php endif; ?>
</div>