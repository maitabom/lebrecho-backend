<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul>

                <li>
                    <b>Versão</b>
                    <?=Cake\Core\Configure::read('system.version') ?>
                </li>

            </ul>
        </nav>
        <div class="copyright pull-right">
            Copyright &copy; 2017
            <?=Cake\Core\Configure::read('author.company') ?>. Todos os direitos reservados.
        </div>
    </div>
</footer>