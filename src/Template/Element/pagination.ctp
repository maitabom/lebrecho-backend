<?php
$opcao_paginacao_number = ['tag' => 'li', 'separator' => '', 'currentTag' => 'a', 'modulus' => 4];
$opcao_paginacao_extra = ['tag' => 'li', 'disabledTag' => 'a'];

if(!isset($limit_pagination))
{
    $limit_pagination = Cake\Core\Configure::read('limitPagination');
}

if (!isset($name))
{
    $name = 'itens';
}

if (!isset($name_singular))
{
    $name_singular = 'item';
}

if (!isset($predicate))
{
    $predicate = 'encontrados';
}

if (!isset($singular))
{
    $singular = 'encontrado';
}
?>
<div class="fixed-table-pagination">
    <?php if ($qtd_total > 0): ?>
        <div class="pull-left pagination-detail">
            <span class="pagination-info"><?= $qtd_total . " " . (($qtd_total == 1) ? $name_singular : $name) . " " . (($qtd_total == 1) ? $singular : $predicate) ?></span>
        </div>
        <?php if ($qtd_total > $limit_pagination): ?>
            <div class="pull-right pagination">
                <ul class="pagination">
                    <?php if(($qtd_total / $limit_pagination) > 5): ?>
                        <?= $this->Paginator->first('«', $opcao_paginacao_extra) ?>
                    <?php endif; ?>
                    <?= $this->Paginator->prev('‹', $opcao_paginacao_extra) ?>
                    <?= $this->Paginator->numbers($opcao_paginacao_number) ?>
                    <?= $this->Paginator->next('›', $opcao_paginacao_extra) ?>
                    <?php if(($qtd_total / $limit_pagination) > 5): ?>
                        <?= $this->Paginator->last('»', $opcao_paginacao_extra) ?>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>