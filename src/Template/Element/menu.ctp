<div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
        <div class="logo">
            <?=$this->Html->link(Cake\Core\Configure::read('system.shortName'), ['controller' => 'system', 'action' => 'board'], ['class' => 'simple-text']) ?>
        </div>

        <ul class="nav">
                <li class="<?=$this->Menu->activeMenu(['controller' => 'System', 'action' => 'board']) ?>">
                    <a href="<?= $this->Url->build(['controller' => 'system', 'action' => 'board']) ?>">
                        <i class="ti-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'add']) ?>">
                        <i class="ti-truck"></i>
                        <p>Novo Produto</p>
                    </a>
                </li>

                <li class="<?=$this->Menu->activeMenu(['controller' => 'Produto', 'action' => 'fotos']) ?>">
                    <a href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'fotos']) ?>">
                        <i class="ti-camera"></i>
                        <p>Fotos</p>
                    </a>
                </li>

                <li class="<?=$this->Menu->activeMenu(['controller' => 'Produto', 'action' => 'index']) ?>">
                    <a href="<?= $this->Url->build('/produto') ?>">
                        <i class="ti-package"></i>
                        <p>Lista de Produtos</p>
                    </a>
                </li>

                <!--
                <li class="<?=$this->Menu->activeMenu(['controller' => 'Produto', 'action' => 'verificacao']) ?>">
                    <a href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'verificacao']) ?>">
                        <i class="ti-write"></i>
                        <p>Verificação</p>
                    </a>
                </li>
                -->
                <li class="<?=$this->Menu->activeMenu(['controller' => 'Produto', 'action' => 'publicacao']) ?>">
                    <a href="<?= $this->Url->build(['controller' => 'produto', 'action' => 'publicacao']) ?>">
                        <i class="ti-export"></i>
                        <p>Publicação</p>
                    </a>
                </li>

        </ul>
    </div>
</div>