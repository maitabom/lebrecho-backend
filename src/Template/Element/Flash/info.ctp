<div class="alert alert-info alert-with-icon">
    <button type="button" aria-hidden="true" title="Fechar" class="close" onclick="$(this).parent().hide('blind')">×</button>
    <span data-notify="icon" class="ti-info-alt"></span>
    <span data-notify="message">
        <?= h($message) ?>
    </span>
</div>