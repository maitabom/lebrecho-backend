<div class="alert alert-danger alert-with-icon" data-notify="container">
    <button type="button" aria-hidden="true" class="close" onclick="$(this).parent().hide('blind')">×</button>
    <span data-notify="icon" class="ti-thumb-down"></span>
    <span data-notify="message">
        <?= h($message) ?>
        &nbsp;&nbsp;<a href="#" onclick="$('#detalhes').toggle('blind');">mais detalhes</a>
    </span>
    <div id="detalhes" class="detalhes">
        <?= h($params['details']) ?>
    </div>
</div>