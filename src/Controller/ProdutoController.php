<?php

namespace App\Controller;


class ProdutoController extends AppController
{
    public function index()
    {
        $combo_status = ["T" => "Todos", "E" => "Somente Ativos", "N" => "Somente Inativos"];
        $status = ["T" => "Todos", "P" => "Publicados", "N" => "Não Publicados"];
        $categorias = ["Todos", "Informática", "Acessórios", "Moda", "Casa e Lazer"];
        $condicoes = ["T" =>"Todos", "N" => "Novo", "S" => "Seminovo", "U" => "Usado"];

        $this->set('title', "Produtos");
        $this->set('combo_status', $combo_status);
        $this->set('categorias', $categorias);
        $this->set('condicoes', $condicoes);
        $this->set('status', $status);

    }

    public function add()
    {
        $this->redirect(['action' => 'cadastro', 0]);
    }

    public function edit(int $id)
    {
        $this->redirect(['action' => 'cadastro', $id]);
    }

    public function cadastro(int $id)
    {
        $fabricantes = ["Lenovo", "Champion", "Positivo", "Orient"];
        $categorias = ["Informática", "Acessórios", "Moda", "Casa e Lazer"];
        $condicoes = ["N" => "Novo", "S" => "Seminovo", "U" => "Usado"];

        $title = ($id > 0) ? "Edição do Produto" : "Novo Produto";

        $this->set('title', $title);
        $this->set('fabricantes', $fabricantes);
        $this->set('categorias', $categorias);
        $this->set('condicoes', $condicoes);
    }

    public function fotos()
    {
        $this->set('title', 'Inserir Imagens do Produto');
    }

    public function imagens(int $id)
    {
        $this->set('title', 'Imagens do Produto');
    }

    public function publicacao()
    {
        $categorias = ["Informática", "Acessórios", "Moda", "Casa e Lazer"];
        $condicoes = ["N" => "Novo", "S" => "Seminovo", "U" => "Usado"];

        $this->set('title', 'Publicar ');
        $this->set('categorias', $categorias);
        $this->set('condicoes', $condicoes);
    }

    public function recepcao()
    {

    }
}